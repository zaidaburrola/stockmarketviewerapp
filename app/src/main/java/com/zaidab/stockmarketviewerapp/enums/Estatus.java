package com.zaidab.stockmarketviewerapp.enums;

public enum Estatus {
    Success,
    JSONError,
    IOError,
    ConnectionError
}
