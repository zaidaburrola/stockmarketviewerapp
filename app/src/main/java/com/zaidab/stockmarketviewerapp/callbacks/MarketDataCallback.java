package com.zaidab.stockmarketviewerapp.callbacks;

import com.zaidab.stockmarketviewerapp.models.MarketData;
import com.zaidab.stockmarketviewerapp.models.Quote;

import java.util.List;

public interface MarketDataCallback {
    /*
    *   Callback for Market Data endpoint
    */
    public void onMarketDataTaskComplete(List<MarketData> marketDataList, Quote quote);

}
