package com.zaidab.stockmarketviewerapp.callbacks;

import com.zaidab.stockmarketviewerapp.enums.Estatus;
import com.zaidab.stockmarketviewerapp.models.Quote;

import java.util.List;

public interface QuoteCallback {
    public void onQuoteTaskComplete(List<Quote> quoteList,Estatus estatus, int requestCode);
    public void onSearchQuoteTask(Quote quote, Estatus estatus);
}
