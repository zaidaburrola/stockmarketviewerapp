package com.zaidab.stockmarketviewerapp.callbacks;

import com.zaidab.stockmarketviewerapp.models.Quote;

public interface QuoteSelectedCallback {
    public void onQuoteSelected(Quote quote);
}
