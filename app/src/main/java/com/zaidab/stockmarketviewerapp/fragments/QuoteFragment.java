package com.zaidab.stockmarketviewerapp.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.zaidab.stockmarketviewerapp.R;
import com.zaidab.stockmarketviewerapp.adapters.QuoteListAdapter;
import com.zaidab.stockmarketviewerapp.callbacks.QuoteSelectedCallback;
import com.zaidab.stockmarketviewerapp.models.Quote;

import java.util.ArrayList;
import java.util.List;

/*
* Fragment to show quotes list
 */
public class QuoteFragment  extends Fragment {

    private View view;
    private ListView mLisView;
    private QuoteListAdapter adapterListView;
    private List<Quote> quoteList;
    QuoteSelectedCallback callback;

    //Receive quote list
    public void setQuoteList(List<Quote> quoteList) {
        this.quoteList = quoteList;
        adapterListView.setQuoteList(quoteList);
        adapterListView.notifyDataSetChanged();
    }

    public void setCallback(QuoteSelectedCallback quoteSelectedCallback){
        this.callback = quoteSelectedCallback;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //init view
        view = inflater.inflate(R.layout.fragment_quotes, container, false);

        mLisView = (ListView) view.findViewById(R.id.recyclerview_top_gainers);
        adapterListView = new QuoteListAdapter(getContext(), quoteList);
        mLisView.setAdapter(adapterListView);
        mLisView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                callback.onQuoteSelected(quoteList.get(i));
            }
        });
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //init quote list
        quoteList = new ArrayList<>();
    }

}
