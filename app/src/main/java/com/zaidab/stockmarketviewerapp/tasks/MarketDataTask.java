package com.zaidab.stockmarketviewerapp.tasks;

import android.os.AsyncTask;

import com.zaidab.stockmarketviewerapp.callbacks.MarketDataCallback;
import com.zaidab.stockmarketviewerapp.models.MarketData;
import com.zaidab.stockmarketviewerapp.models.Quote;
import com.zaidab.stockmarketviewerapp.utils.HttpConnectionUtil;
import com.zaidab.stockmarketviewerapp.utils.JSONUtil;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MarketDataTask  extends AsyncTask<String, Void, List<MarketData>> {


    MarketDataCallback callback;
    Quote quote;

    public MarketDataTask(MarketDataCallback callback, Quote quote){
        this.callback = callback;
        this.quote = quote;
    }

    /*
     *   Connect to Api to get market data  json array list,
     *   return parsed market data list
     */

    @Override
    protected List<MarketData> doInBackground(String... params) {
        //get endpoint
        String endpoint = params[0];

        List<MarketData> marketDataList = new ArrayList<>();
        HttpConnectionUtil httpConnectionUtil = new HttpConnectionUtil();
        JSONArray jsonResult = httpConnectionUtil.getQuotesRequest(endpoint);
        try {
            marketDataList = JSONUtil.getMarketData(jsonResult);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return marketDataList;
    }

    @Override
    protected void onPostExecute(List<MarketData> marketDataList) {
        super.onPostExecute(marketDataList);
        callback.onMarketDataTaskComplete(marketDataList, quote);
    }
}