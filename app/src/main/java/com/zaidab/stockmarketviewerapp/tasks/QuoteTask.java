package com.zaidab.stockmarketviewerapp.tasks;

import android.os.AsyncTask;

import com.zaidab.stockmarketviewerapp.callbacks.QuoteCallback;
import com.zaidab.stockmarketviewerapp.enums.Estatus;
import com.zaidab.stockmarketviewerapp.models.Quote;
import com.zaidab.stockmarketviewerapp.utils.HttpConnectionUtil;
import com.zaidab.stockmarketviewerapp.utils.JSONUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class QuoteTask extends AsyncTask<String, Void, Quote> {


    QuoteCallback callback;
    Estatus estatus;

    public QuoteTask(QuoteCallback callback){
        this.callback = callback;
    }


    /*
    *   Connect to Api to get quotes json array list,
    *   return parsed quotes list
    */

    @Override
    protected Quote doInBackground(String... params) {
        //get endpoint
        String endpoint = params[0];

        //Init quoute
        Quote quote = new Quote();
        //Init connection
        HttpConnectionUtil httpConnectionUtil = new HttpConnectionUtil();
        // Json request
        try {
            JSONObject jsonResult = httpConnectionUtil.getQuoteRequest(endpoint);
            //Parser json
            quote = JSONUtil.getQuote(jsonResult);
            estatus = Estatus.Success;
        } catch (JSONException e) {
            e.printStackTrace();
            estatus = Estatus.JSONError;
        } catch (IOException e) {
            e.printStackTrace();
            estatus = Estatus.IOError;
        }


        return quote;
    }

    @Override
    protected void onPostExecute(Quote quote) {
        super.onPostExecute(quote);
        //Send
        callback.onSearchQuoteTask(quote, estatus);
    }
}
