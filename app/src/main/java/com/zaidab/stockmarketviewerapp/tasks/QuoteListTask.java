package com.zaidab.stockmarketviewerapp.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.zaidab.stockmarketviewerapp.callbacks.QuoteCallback;
import com.zaidab.stockmarketviewerapp.enums.Estatus;
import com.zaidab.stockmarketviewerapp.models.Quote;
import com.zaidab.stockmarketviewerapp.utils.HttpConnectionUtil;
import com.zaidab.stockmarketviewerapp.utils.JSONUtil;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class QuoteListTask  extends AsyncTask<String, Void, List<Quote>> {


    private QuoteCallback callback;
    private int requestCode;
    private Estatus estatus;
    private Context mContext;

    public QuoteListTask(QuoteCallback callback, Context mContext, int requestCode){
        this.callback = callback;
        this.requestCode = requestCode;
        this.mContext = mContext;
    }

    /*
     *   Connect to Api to get quotes json array list,
     *   return parsed quotes list
     */

    @Override
    protected List<Quote> doInBackground(String... params) {
        //Init list of quoutes
        List<Quote> quoteList = new ArrayList<>();
        //Check internet connection
        if(HttpConnectionUtil.isInternetAvailable(mContext)){
            //get endpoint
            String endpoint = params[0];

            //Init connection
            HttpConnectionUtil httpConnectionUtil = new HttpConnectionUtil();
            // Json request
            JSONArray jsonResult = httpConnectionUtil.getQuotesRequest(endpoint);
            try {
                //Parser json
                quoteList = JSONUtil.getQuoteList(jsonResult);
                estatus = Estatus.Success;
            } catch (JSONException e) {
                estatus = Estatus.JSONError;
                e.printStackTrace();
            }
        }else{
            estatus = Estatus.ConnectionError;
        }




        return quoteList;
    }

    @Override
    protected void onPostExecute(List<Quote> quotes) {
        super.onPostExecute(quotes);
        //Send
        callback.onQuoteTaskComplete(quotes, estatus, requestCode);
    }
}
