package com.zaidab.stockmarketviewerapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zaidab.stockmarketviewerapp.R;
import com.zaidab.stockmarketviewerapp.models.Quote;

import java.util.ArrayList;
import java.util.List;

public class QuoteListAdapter extends BaseAdapter {

    List<Quote> quoteList;
    Context mContext;

    public QuoteListAdapter(Context mContext, List<Quote> quotes){
        super();
        this.mContext = mContext;
        this.quoteList = quotes;
    }

    @Override
    public int getCount() {
        return quoteList.size();
    }

    @Override
    public Object getItem(int i) {
        return quoteList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemViewHolder holder = null;
        if(convertView == null) {
            holder = new ItemViewHolder();
            convertView = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_quote, parent,false);
            holder.txtSymbol = (TextView) convertView.findViewById(R.id.quote_symbol);
            holder.txtNameCompany = (TextView) convertView.findViewById(R.id.quote_company_name);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        holder.txtSymbol.setText(quoteList.get(position).getSymbol());
        holder.txtNameCompany.setText(quoteList.get(position).getCompanyName());
        return convertView;
    }

    private static class ItemViewHolder {
        TextView txtSymbol;
        TextView txtNameCompany;
    }


    public void setQuoteList(List<Quote> quoteList) {
        this.quoteList = quoteList;
    }
}
