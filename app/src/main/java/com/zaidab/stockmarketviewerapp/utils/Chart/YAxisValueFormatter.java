package com.zaidab.stockmarketviewerapp.utils.Chart;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.zaidab.stockmarketviewerapp.models.MarketData;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class YAxisValueFormatter  extends ValueFormatter {
    DecimalFormat currencyFormat  = new DecimalFormat("'$'0.00");



    public YAxisValueFormatter() {
    }

    @Override
    public String getPointLabel(Entry entry) {
        return currencyFormat.format(entry.getY());
    }

    @Override
    public String getBarLabel(BarEntry barEntry) {
        return currencyFormat.format(barEntry.getY());
    }

    @Override
    public String getAxisLabel(float value, AxisBase axis) {

        // "value" represents the position of the label on the axis (x or y)
        //return mValues[(int) value];
        return currencyFormat.format(value);
    }
}
