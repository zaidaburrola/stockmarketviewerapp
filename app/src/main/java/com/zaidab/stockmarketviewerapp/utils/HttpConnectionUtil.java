package com.zaidab.stockmarketviewerapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.zaidab.stockmarketviewerapp.models.Quote;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

//Todo cambiar nombre a HttpUtil
public class HttpConnectionUtil {


    public JSONArray getQuotesRequest(String url) {
        JSONArray jsonArrayResult = new JSONArray();

        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) getConnection(url);
            if(httpURLConnection.getResponseCode() == httpURLConnection.HTTP_OK){

                InputStream is = new BufferedInputStream(httpURLConnection.getInputStream());
                BufferedReader br= new BufferedReader(new InputStreamReader(is));
                String dataJson;
                StringBuffer jsonData = new StringBuffer();
                while((dataJson = br.readLine())!= null){
                    jsonArrayResult = new JSONArray(dataJson);
                }
                br.close();
                is.close();

                //
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArrayResult;

    }

    private HttpURLConnection getConnection(String url) throws IOException {
            URL endpoint = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) endpoint.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(2000);
            conn.setReadTimeout(2000);
            conn.setDoInput(true);
            return conn;
    }


    public static boolean isInternetAvailable(Context mContext) {
        ConnectivityManager cm =
            (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public JSONObject getQuoteRequest(String endpoint) throws IOException, JSONException {
        JSONObject jsonResult = new JSONObject();

        HttpURLConnection httpURLConnection = null;
        httpURLConnection = (HttpURLConnection) getConnection(endpoint);
        if(httpURLConnection.getResponseCode() == httpURLConnection.HTTP_OK){

            InputStream is = new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader br= new BufferedReader(new InputStreamReader(is));
            String dataJson;
            while((dataJson = br.readLine())!= null){
                jsonResult = new JSONObject(dataJson);
            }
            br.close();
            is.close();

            //
        }


        return jsonResult;
    }
}
