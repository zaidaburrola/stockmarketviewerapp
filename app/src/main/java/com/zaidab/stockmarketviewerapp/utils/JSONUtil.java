package com.zaidab.stockmarketviewerapp.utils;

import com.zaidab.stockmarketviewerapp.models.MarketData;
import com.zaidab.stockmarketviewerapp.models.Quote;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
/*
* Class to parse jsons
*/
public class JSONUtil {

    /*
    * receive JSONArray of quotes and
    * return parsed quotes list
    */
    public static List<Quote> getQuoteList(JSONArray jsonQuotes) throws JSONException {
        List<Quote> quoteList = new ArrayList<>();

        for (int i = 0; i < jsonQuotes.length(); i++) {
            JSONObject jsonQuote = jsonQuotes.getJSONObject(i);
            Quote quote = new Quote();
            quote.setCompanyName(jsonQuote.getString("companyName"));
            quote.setSymbol(jsonQuote.getString("symbol"));
            quote.setSector(jsonQuote.getString("sector"));
            quoteList.add(quote);
        }

        return quoteList;
    }


    /*
     * receive JSONArray of merket data and
     * return parsed market data list
     */
    public static List<MarketData>  getMarketData(JSONArray jsonResult) throws JSONException {
        List<MarketData> marketDataList =  new ArrayList<>();

        for (int i = 0; i < jsonResult.length(); i++) {
            MarketData marketData = new MarketData();
            JSONObject jsonQuote = jsonResult.getJSONObject(i);
            marketData.setDate(jsonQuote.getString("date"));
            marketData.setLabel(jsonQuote.getString("label"));
            marketData.setHigh(jsonQuote.getDouble("high"));
            marketData.setLow(jsonQuote.getDouble("low"));

            marketDataList.add(marketData);

        }

        return marketDataList;
    }

    public static Quote getQuote(JSONObject jsonQuote) throws JSONException {

        Quote quote = new Quote();
        quote.setCompanyName(jsonQuote.getString("companyName"));
        quote.setSymbol(jsonQuote.getString("symbol"));
        quote.setSector(jsonQuote.getString("sector"));

        return quote;
    }
}
