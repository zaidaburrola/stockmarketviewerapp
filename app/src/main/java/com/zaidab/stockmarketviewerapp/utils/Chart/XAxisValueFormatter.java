package com.zaidab.stockmarketviewerapp.utils.Chart;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.zaidab.stockmarketviewerapp.models.MarketData;

import java.util.List;

public class XAxisValueFormatter extends ValueFormatter {

    private List<MarketData> mValues;

    public XAxisValueFormatter(List<MarketData> values) {
        this.mValues = values;
    }

    @Override
    public String getAxisLabel(float value, AxisBase axis) {
        // "value" represents the position of the label on the axis (x or y)
        //return mValues[(int) value];
        return mValues.get((int)value).getLabel();
    }

}
