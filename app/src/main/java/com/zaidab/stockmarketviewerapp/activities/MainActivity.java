package com.zaidab.stockmarketviewerapp.activities;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.zaidab.stockmarketviewerapp.R;
import com.zaidab.stockmarketviewerapp.adapters.ViewPagerAdapter;
import com.zaidab.stockmarketviewerapp.callbacks.MarketDataCallback;
import com.zaidab.stockmarketviewerapp.callbacks.QuoteCallback;
import com.zaidab.stockmarketviewerapp.callbacks.QuoteSelectedCallback;
import com.zaidab.stockmarketviewerapp.dialogs.QuoteDialog;
import com.zaidab.stockmarketviewerapp.enums.Estatus;
import com.zaidab.stockmarketviewerapp.fragments.QuoteFragment;
import com.zaidab.stockmarketviewerapp.models.MarketData;
import com.zaidab.stockmarketviewerapp.models.Quote;
import com.zaidab.stockmarketviewerapp.tasks.MarketDataTask;
import com.zaidab.stockmarketviewerapp.tasks.QuoteListTask;
import com.zaidab.stockmarketviewerapp.tasks.QuoteTask;

import java.util.List;

public class MainActivity extends AppCompatActivity implements
        QuoteCallback, MarketDataCallback, QuoteSelectedCallback {
    
    // Tab positions
    private static final int TAB_TOP_GAINERS = 0;
    private static final int TAB_TOP_ACTIVES = 1;

    private Context mContext;
    private TabLayout mTablayout;
    private ViewPagerAdapter mViewPagerAdapter;
    private ProgressBar mProgressBar;
    private ViewPager mViewPager;
    private QuoteFragment topGainersFragment;
    private QuoteFragment topActivesFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = getBaseContext();
        //Init view elements
        mTablayout = (TabLayout) findViewById(R.id.tablayout);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //Control tabs selection
        mTablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            /*
            *   If tab position is 0, consume the endpoint of top gainer's
            *   quotes, otherwise if tab position is 1, consume endpoint 
            *   of top active quotes.
            */
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int tabPosition = tab.getPosition();
                switch (tabPosition){
                    case TAB_TOP_GAINERS:
                        //Show progress bar
                        mProgressBar.setVisibility(View.VISIBLE);
                        new QuoteListTask(MainActivity.this,mContext, TAB_TOP_GAINERS)
                                .execute(getString(R.string.top_gainers_endpoint));
                        break;
                    case TAB_TOP_ACTIVES:
                        //show progress bar
                        mProgressBar.setVisibility(View.VISIBLE);
                        new QuoteListTask(MainActivity.this,mContext, TAB_TOP_ACTIVES)
                                .execute(getString(R.string.top_actives_endpoint));
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });



        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        //Init Fragments
        topGainersFragment = new QuoteFragment();
        topActivesFragment = new QuoteFragment();
        //Assign ViewPagerAdapter
        mViewPagerAdapter.AddFragment(topGainersFragment, getString(R.string.title_tab1));
        mViewPagerAdapter.AddFragment(topActivesFragment, getString(R.string.title_tab2));

        //Assign viewpagerAdapter to Viewpage
        mViewPager.setAdapter(mViewPagerAdapter);
        //Assign View page to TabLayout
        mTablayout.setupWithViewPager(mViewPager);

        //Remove shadow from the action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(0);

        //showQuoteDialog();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        if (fragment instanceof QuoteFragment) {
            topGainersFragment.setCallback(this);
            topActivesFragment.setCallback(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search, menu);

        //Init search manager
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem mSearchmenuItem = menu.findItem(R.id.menu_toolbarsearch);
        SearchView searchView = (SearchView) mSearchmenuItem.getActionView();
        searchView.setQueryHint(getString(R.string.search_viewer_hint));

        //control searching 
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String symbol) {

                //Show progress bar
                mProgressBar.setVisibility(View.VISIBLE);
                //Search for symbol
                new QuoteTask(MainActivity.this).execute(getString(R.string.search_symbol_endpoint, symbol));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });


        return true;
    }
    //Receive the endpoint response
    @Override
    public void onQuoteTaskComplete(List<Quote> quoteList,Estatus estatus, int requestCode) {
        //hide progress barr
        mProgressBar.setVisibility(View.GONE);
        switch (estatus){
            case Success:
                /*
                 *   If requestCode is 0, update TAB_TOP_GAINERS,
                 *   otherwise if requestCode is 1, update TAB_TOP_ACTIVES
                 */
                switch (requestCode){
                    case TAB_TOP_GAINERS:
                        topGainersFragment.setQuoteList(quoteList);
                        break;
                    case TAB_TOP_ACTIVES:
                        topActivesFragment.setQuoteList(quoteList);
                        break;
                }
                break;
            case ConnectionError:
                showInternetErrorAlert();
                break;
        }



    }

    @Override
    public void onSearchQuoteTask(Quote quote, Estatus estatus) {
        mProgressBar.setVisibility(View.GONE);
        switch (estatus){
            case Success:
                //Get market data
                mProgressBar.setVisibility(View.VISIBLE);
                new MarketDataTask(this, quote).execute(getString(R.string.historically_market_data_endpoint, quote.getSymbol()));
                break;

                default:
                    showQuoteSymbolErrorAlert();
                    break;
        }
    }

    private void showQuoteDialog(List<MarketData> marketData, Quote quote) {
        FragmentManager fm = getSupportFragmentManager();
        QuoteDialog editNameDialogFragment = QuoteDialog.newInstance("Some Title");
        editNameDialogFragment.setQuote(quote);
        editNameDialogFragment.setMarketDataList(marketData);
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    /*
    * Receive market data
     */

    @Override
    public void onMarketDataTaskComplete(List<MarketData> marketDataList, Quote quote) {
        mProgressBar.setVisibility(View.GONE);
        showQuoteDialog(marketDataList, quote);
    }

    @Override
    public void onQuoteSelected(Quote quote) {
        //Toast.makeText(mContext, "quote: "+quote.symbol, Toast.LENGTH_SHORT).show();
        mProgressBar.setVisibility(View.VISIBLE);
        new MarketDataTask(this, quote).execute(getString(R.string.historically_market_data_endpoint, quote.getSymbol()));
    }




    private  void showInternetErrorAlert(){
        AlertDialog.Builder dialog=new AlertDialog.Builder(this);
        dialog.setMessage("Please check your internet connection and try again");
        dialog.setTitle("Connection error");
        dialog.setPositiveButton("Try again",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        //Show progress bar
                        mProgressBar.setVisibility(View.VISIBLE);
                        new QuoteListTask(MainActivity.this,mContext, TAB_TOP_GAINERS)
                                .execute(getString(R.string.top_gainers_endpoint));
                    }
                });
        AlertDialog alertDialog=dialog.create();
        alertDialog.show();
    }

    private  void showQuoteSymbolErrorAlert(){
        AlertDialog.Builder dialog=new AlertDialog.Builder(this);
        dialog.setTitle("Symbol not found");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog alertDialog=dialog.create();
        alertDialog.show();
    }

}
