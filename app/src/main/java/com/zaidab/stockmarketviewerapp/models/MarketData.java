package com.zaidab.stockmarketviewerapp.models;

public class MarketData {
    public String date;
    public String label;
    public double high;
    public double low;

    public MarketData() {
    }

    public MarketData(String date, String label, double high, double low) {
        this.date = date;
        this.label = label;
        this.high = high;
        this.low = low;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }
}
