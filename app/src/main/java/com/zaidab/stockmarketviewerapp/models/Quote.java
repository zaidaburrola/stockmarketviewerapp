package com.zaidab.stockmarketviewerapp.models;

public class Quote {

    public String symbol;
    public String companyName;
    public String sector;

    public  Quote(){}

    public Quote(String symbol, String companyName, String sector) {
        this.symbol = symbol;
        this.companyName = companyName;
        this.sector = sector;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }
}
