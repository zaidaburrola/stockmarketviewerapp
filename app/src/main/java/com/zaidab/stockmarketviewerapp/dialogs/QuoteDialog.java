package com.zaidab.stockmarketviewerapp.dialogs;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.zaidab.stockmarketviewerapp.R;
import com.zaidab.stockmarketviewerapp.models.MarketData;
import com.zaidab.stockmarketviewerapp.models.Quote;
import com.zaidab.stockmarketviewerapp.utils.Chart.XAxisValueFormatter;
import com.zaidab.stockmarketviewerapp.utils.Chart.YAxisValueFormatter;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class QuoteDialog  extends DialogFragment {

    private TextView mTxtCompanyName;
    private TextView mTxtSymbol;
    private TextView mTxtSector;
    private LineChart mChart;
    private Quote quote;
    private List<MarketData> marketDataList;

    public QuoteDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    //setters

    public void setQuote(Quote quote) {
        this.quote = quote;
    }

    public void setMarketDataList(List<MarketData> marketDataList) {
        this.marketDataList = marketDataList;
    }

    public static QuoteDialog newInstance(String title) {
        QuoteDialog frag = new QuoteDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_quote, container);
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        // Set the width of the dialog proportional to 90% of the screen width
        // Set the high of the dialog proportional to 75% of the screen high

        window.setLayout((int) (size.x * 0.90), (int) (size.y * 0.75));
        window.setGravity(Gravity.CENTER);

        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        mTxtCompanyName = (TextView) view.findViewById(R.id.txt_quote_company_name);
        mTxtSymbol = (TextView) view.findViewById(R.id.txt_quote_symbol);
        mTxtSector = (TextView) view.findViewById(R.id.txt_quote_sector);
        mChart = (LineChart) view.findViewById(R.id.chart_market_data);

        //Assign Data
        mTxtSymbol.setText(quote.getSymbol());
        mTxtCompanyName.setText(quote.getCompanyName());
        mTxtSector.setText("Sector: "+quote.getSector());


        initChart();

        //Populate chart
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();

        //high
        ArrayList<Entry> values = new ArrayList<>();

        for (int i = 0; i < marketDataList.size(); i++) {
            double val = marketDataList.get(i).getHigh();
            values.add(new Entry(i, (float) val));
        }
        LineDataSet d = new LineDataSet(values, "high");
        d.setValueFormatter(new YAxisValueFormatter());
        d.setLineWidth(2.5f);
        d.setCircleRadius(4f);

        d.setColor(getResources().getColor(R.color.colorChartHigh));
        d.setCircleColor(getResources().getColor(R.color.colorChartCircleHigh));
        dataSets.add(d);


        //low
        ArrayList<Entry> valuesLow = new ArrayList<>();

        for (int i = 0; i < marketDataList.size(); i++) {
            double val = marketDataList.get(i).getLow();
            valuesLow.add(new Entry(i, (float) val));
        }
        LineDataSet dl = new LineDataSet(valuesLow, "low");
        dl.setLineWidth(2.5f);
        dl.setValueFormatter(new YAxisValueFormatter());
        dl.setCircleRadius(4f);

        dl.setColor(getResources().getColor(R.color.colorChartLow));
        dl.setCircleColor(getResources().getColor(R.color.colorChartCircleLow));
        dataSets.add(dl);


        LineData data = new LineData(dataSets);
        mChart.setData(data);
        mChart.invalidate();


        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

    }


    private void initChart(){
        //Init chart
        mChart.setDrawGridBackground(false);
        mChart.getDescription().setEnabled(false);
        mChart.setDrawBorders(false);

        mChart.getAxisLeft().setEnabled(false);
        mChart.getAxisRight().setDrawAxisLine(false);
        mChart.getAxisRight().setDrawGridLines(false);
        mChart.getXAxis().setDrawAxisLine(false);
        mChart.getXAxis().setDrawGridLines(false);
        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        // Scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        //Legends
        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);

        //Update xAxis

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new XAxisValueFormatter(marketDataList));

        /* Update YAxis */

        YAxis yAxis = mChart.getAxisRight();
        yAxis.setValueFormatter(new YAxisValueFormatter());
    }

}